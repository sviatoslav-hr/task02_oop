package com.khrystyna.models;

import java.util.Objects;

public class WashingPowder extends HouseholdChemical {
    private String washingType;
    private int washingNumber;
    private String function;
    private double capacity;

    public String getWashingType() {
        return washingType;
    }

    public void setWashingType(String washingType) {
        this.washingType = washingType;
    }

    public int getWashingNumber() {
        return washingNumber;
    }

    public void setWashingNumber(int washingNumber) {
        this.washingNumber = washingNumber;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WashingPowder that = (WashingPowder) o;
        return washingNumber == that.washingNumber &&
                Double.compare(that.capacity, capacity) == 0 &&
                Objects.equals(washingType, that.washingType) &&
                Objects.equals(function, that.function);
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(washingType, washingNumber, function, capacity);
    }

    @Override
    public String toString() {
        return getBrand() + " " + function
                + " " + capacity + " " + getPrice() + '$';
    }

    @Override
    public String toStringFull() {
        return "Brand: " + getBrand()
                + "\nCapacity: " + capacity + 'L'
                + "\nWashing type: " + washingType
                + "\nWashing number: " + washingNumber
                + "\nFunction: " + function
                + "\nPrice: " + getPrice();
    }
}
