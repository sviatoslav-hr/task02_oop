package com.khrystyna.views;

import com.khrystyna.controllers.ChemicalController;
import com.khrystyna.models.HouseholdChemical;

import java.util.List;
import java.util.Scanner;

/**
 * An instance of this class is used to communicate with user by console line.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 03.04.2019
 */
public class ConsoleView {
    /**
     * Scanner that allows to read an input from System.in.
     */
    private Scanner scanner = new Scanner(System.in);
    /**
     * Controller for connections with StoreService.
     */
    private ChemicalController controller = new ChemicalController();

    /**
     * Main method that run View.
     */
    public final void run() {
        displayMenu();
    }

    /**
     * Prints main menu.
     */
    private void displayMenu() {
        System.out.print("---Household Chemicals---"
                + "\n1 - Show all products"
                + "\n2 - Select product category"
                + "\n0 - exit"
                + "\n\nChoose option: ");
        int choice = getIntegerInput();
        switch (choice) {
            case 1:
                printAllProducts();
                break;
            case 2:
                displayCategories();
                break;
            case 0:
                return;
            default:
                System.out.println("Incorrect input!");
                displayMenu();
        }
    }

    /**
     * Prints all the available categories and asks to select one of them.
     */
    private void displayCategories() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("---Household Chemicals Categories---");

        String[] categories = controller.getCategories();

        for (int i = 0; i < categories.length; i++) {
            stringBuilder.append('\n')
                    .append(i + 1).append('\t')
                    .append(categories[i]);
        }
        System.out.print(stringBuilder
                + "\n\nEnter category number to select or "
                + "'0' to return to menu: ");

        int choice = getIntegerInput();

        if (choice == 0) {
            displayMenu();
        } else if (choice < categories.length && choice > 0) {
            displayProductsByCategory(categories[choice - 1]);
        } else {
            System.out.println("Incorrect input!");
            displayCategories();
        }

    }

    /**
     * Prints all products from selected category.
     *
     * @param category category of products
     */
    private void displayProductsByCategory(final String category) {
        List<HouseholdChemical> chemicals = controller
                .getChemicalsByCategory(category);
        System.out.println("---Household Chemicals / " + category + "---");
        printProducts(chemicals);
    }

    /**
     * Prints all the products.
     */
    private void printAllProducts() {
        List<HouseholdChemical> chemicals = controller.getAllChemicals();
        System.out.print("\n---Household Chemicals---");
        printProducts(chemicals);
    }

    /**
     * Prints an entry product list.
     *
     * @param chemicals product list
     */
    private void printProducts(final List<HouseholdChemical> chemicals) {
        StringBuilder stringBuilder = new StringBuilder();

        for (HouseholdChemical chemical : chemicals) {
            stringBuilder.append('\n')
                    .append(chemical.getId()).append('\t')
                    .append(chemical);
        }

        System.out.print(stringBuilder
                + "\n\nEnter product ID to select or '0' to return to menu: ");
        int choice = getIntegerInput();

        if (choice == 0) {
            displayMenu();
        } else {
            displayProduct(choice);
        }
    }

    /**
     * Prints single product by id and asks to order.
     *
     * @param chemicalId product id
     */
    private void displayProduct(final int chemicalId) {
        HouseholdChemical chemical = controller.getChemicalById(chemicalId);

        System.out.println("\n---Household Chemicals---\n"
                + chemical.toStringFull());

        System.out.print("\n1 - Order"
                + "\n0 - Go back"
                + "\n\nSelect option: ");

        int input = getIntegerInput();

        switch (input) {
            case 1:
                boolean result = controller.processOrder(chemicalId);
                if (result) {
                    System.out.println("Ordered successfully\n");
                    displayMenu();
                } else {
                    System.out.println("Product not found!");
                }
                return;
            case 0:
                return;
            default:
                System.out.println("Incorrect input!");
        }
    }

    /**
     * @return int value typed from console.
     */
    private int getIntegerInput() {
        return scanner.nextInt();
    }
}
