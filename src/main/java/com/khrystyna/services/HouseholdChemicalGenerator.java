package com.khrystyna.services;

import com.khrystyna.models.CleaningProduct;
import com.khrystyna.models.DishwashingDetergent;
import com.khrystyna.models.FabricSoftener;
import com.khrystyna.models.HouseholdChemical;
import com.khrystyna.models.WashingPowder;
import com.khrystyna.models.Whitener;

import java.util.Random;

/**
 * An instance of this class if used to generate
 * pseudorandom {@link HouseholdChemical} child objects.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 03.04.2019
 */
public class HouseholdChemicalGenerator {
    /**
     * An instance that is used to generate a stream of pseudorandom numbers.
     */
    private Random random = new Random();

    // Constants for generating objects
    private final String[] productBrands = {"E", "LOSK", "Lenor", "Frosch",
            "Yplon", "Somat", "ACE", "Bref"};
    private final String[] placesOfUse = {"Glass", "Floor", "Drain Pipes"};
    private final String[] washingTypes = {"Universal", "Machine washing",
            "Handwashing"};
    private final String[] washingPowderFunctions = {"for Color fabric",
            "for White fabric"};
    private final String[] whitenerDetergentTypes = {"Soap", "Gel", "Powder",
            "Liquid", "Spray"};
    private final String[] whitenerFabricTypes = {"All", "Cotton", "Silk",
            "Baby", "Delicate"};
    private final double minCapacity = 0.25;
    private final double maxCapacity = 5.0;

    /**
     * Gets filled instance of random {@link HouseholdChemical} child.
     *
     * @return random HouseholdChemical child
     */
    public final HouseholdChemical getRandomChemical() {
        final int chemicalTypesNumber = 5;
        int randomType = random.nextInt(chemicalTypesNumber);

        switch (randomType) {
            case 0:
                return getRandomCleaningProduct();
            case 1:
                return getRandomDishwashingDetergent();
            case 2:
                return getRandomFabricSoftener();
            case 3:
                return getRandomWashingPowder();
            case 4:
                return getRandomWhitener();
            default:
                return null;
        }
    }

    /**
     * Get randomly filled {@link CleaningProduct} instance.
     *
     * @return random CleaningProduct instance
     */
    private CleaningProduct getRandomCleaningProduct() {
        CleaningProduct cleaningProduct = new CleaningProduct();
        fillChemical(cleaningProduct);

        cleaningProduct.setCapacity(minCapacity
                * (1 + random.nextInt((int) (maxCapacity / minCapacity))));
        cleaningProduct.setPlaceOfUse(
                placesOfUse[random.nextInt(placesOfUse.length)]);
        cleaningProduct.setCategory("Cleaning products");

        return cleaningProduct;
    }

    /**
     * Get randomly filled {@link DishwashingDetergent} instance.
     *
     * @return random DishwashingDetergent instance
     */
    private DishwashingDetergent getRandomDishwashingDetergent() {
        DishwashingDetergent dishwashingDetergent = new DishwashingDetergent();
        fillChemical(dishwashingDetergent);

        dishwashingDetergent.setCapacity(minCapacity
                * (1 + random.nextInt((int) (maxCapacity / minCapacity))));
        dishwashingDetergent.setCategory("Dishwashing detergents");

        return dishwashingDetergent;
    }

    /**
     * Get randomly filled {@link FabricSoftener} instance.
     *
     * @return random FabricSoftener instance
     */
    private FabricSoftener getRandomFabricSoftener() {
        FabricSoftener fabricSoftener = new FabricSoftener();
        fillChemical(fabricSoftener);

        fabricSoftener.setCapacity(minCapacity
                * (1 + random.nextInt((int) (maxCapacity / minCapacity))));
        fabricSoftener.setWashingType(
                washingTypes[random.nextInt(washingTypes.length)]);
        fabricSoftener.setCategory("Fabric softeners");

        return fabricSoftener;
    }

    /**
     * Get randomly filled {@link WashingPowder} instance.
     *
     * @return random WashingPowder instance
     */
    private WashingPowder getRandomWashingPowder() {
        WashingPowder washingPowder = new WashingPowder();
        fillChemical(washingPowder);

        washingPowder.setCapacity(minCapacity
                * (1 + random.nextInt((int) (maxCapacity / minCapacity))));
        washingPowder.setWashingType(
                washingTypes[random.nextInt(washingTypes.length)]);
        washingPowder.setFunction(
                washingPowderFunctions[random
                        .nextInt(washingPowderFunctions.length)]);
        washingPowder.setCategory("Washing powders");

        return washingPowder;
    }

    /**
     * Get randomly filled {@link Whitener} instance.
     *
     * @return random Whitener instance
     */
    private Whitener getRandomWhitener() {
        Whitener whitener = new Whitener();
        fillChemical(whitener);

        whitener.setWeight(minCapacity
                * (1 + random.nextInt((int) (maxCapacity / minCapacity))));
        whitener.setDetergentType(
                whitenerDetergentTypes[random
                        .nextInt(whitenerDetergentTypes.length)]);
        whitener.setFabricType(
                whitenerFabricTypes[random
                        .nextInt(whitenerFabricTypes.length)]);
        whitener.setCategory("Whiteners");

        return whitener;
    }

    /**
     * Pseudorandomly fills {@link HouseholdChemical} child.
     *
     * @param chemical HouseholdChemical child
     */
    private void fillChemical(final HouseholdChemical chemical) {
        final int maxPrice = 200;
        final double priceDivider = 10.0;
        chemical.setBrand(
                productBrands[random.nextInt(productBrands.length)]);
        chemical.setPrice(1 + (random.nextInt(maxPrice) / priceDivider));
    }
}
