package com.khrystyna.models;

import java.util.Objects;

public class CleaningProduct extends HouseholdChemical {
    private double capacity;
    private String placeOfUse;

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public String getPlaceOfUse() {
        return placeOfUse;
    }

    public void setPlaceOfUse(String placeOfUse) {
        this.placeOfUse = placeOfUse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CleaningProduct that = (CleaningProduct) o;
        return Double.compare(that.capacity, capacity) == 0 &&
                placeOfUse.equals(that.placeOfUse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, placeOfUse);
    }

    @Override
    public String toString() {
        return getBrand() + " " + getCapacity() + "L \t" + getPrice() + '$';
    }

    @Override
    public String toStringFull() {
        return "Brand: " + getBrand()
                + "\nCapacity: " + getCapacity() + 'L'
                + "\nPlace of use: " + getPlaceOfUse()
                + "\nPrice: " + getPrice();
    }
}
