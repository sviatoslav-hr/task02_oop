package com.khrystyna.models;

import java.util.Objects;

public class FabricSoftener extends HouseholdChemical {
    private double capacity;
    private String washingType;

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public String getWashingType() {
        return washingType;
    }

    public void setWashingType(String washingType) {
        this.washingType = washingType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FabricSoftener that = (FabricSoftener) o;
        return Double.compare(that.capacity, capacity) == 0 &&
                Objects.equals(washingType, that.washingType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, washingType);
    }

    @Override
    public String toString() {
        return getBrand() + " " + capacity + " " + getPrice() + '$';
    }

    @Override
    public String toStringFull() {
        return "Brand: " + getBrand()
                + "\nCapacity: " + getCapacity() + 'L'
                + "\nWashing type: " + getWashingType()
                + "\nPrice: " + getPrice();
    }
}
