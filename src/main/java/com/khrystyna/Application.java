package com.khrystyna;

import com.khrystyna.services.HouseholdChemicalGenerator;
import com.khrystyna.services.StoreService;
import com.khrystyna.views.ConsoleView;

/**
 * Main service.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 04.04.2019
 */
public class Application {

    /**
     * Utility class should not have a public or default constructor.
     */
    private Application() {
    }

    /**
     * Number of products to fill database.
     */
    private static final int PRODUCTS_NUMBER = 30;

    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(final String[] args) {

        prepareDb();

        ConsoleView view = new ConsoleView();
        view.run();
    }

    /**
     * Prepares mock database and fills it with random values.
     */
    private static void prepareDb() {
        StoreService storeService = StoreService.getInstance();
        HouseholdChemicalGenerator generator = new HouseholdChemicalGenerator();

        for (int i = 0; i < PRODUCTS_NUMBER; i++) {
            storeService.addChemical(generator.getRandomChemical());
        }
    }
}
