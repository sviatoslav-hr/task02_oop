package com.khrystyna.models;

import java.util.Objects;

public class Whitener extends HouseholdChemical {
    private double weight;
    private String detergentType;
    private String fabricType;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getDetergentType() {
        return detergentType;
    }

    public void setDetergentType(String detergentType) {
        this.detergentType = detergentType;
    }

    public String getFabricType() {
        return fabricType;
    }

    public void setFabricType(String fabricType) {
        this.fabricType = fabricType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Whitener whitener = (Whitener) o;
        return Double.compare(whitener.weight, weight) == 0 &&
                Objects.equals(detergentType, whitener.detergentType) &&
                Objects.equals(fabricType, whitener.fabricType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, detergentType, fabricType);
    }


    @Override
    public String toString() {
        return getBrand() + " for " + fabricType
                + " fabric " + weight + "g " + getPrice() + '$';
    }

    @Override
    public String toStringFull() {
        return "Brand: " + getBrand()
                + "\nWeight: " + weight + 'g'
                + "\nDetergent type: " + detergentType
                + "\nFabric type: " + fabricType
                + "\nPrice: " + getPrice();
    }
}
