package com.khrystyna.services;

import com.khrystyna.models.HouseholdChemical;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A simple service that stores products.
 *
 * @author Sviatoslav Khrystyna
 * @version 1.0
 * @since 03.04.2019
 */
public class StoreService {
    /**
     * Main list that holds all the chemicals.
     */
    private List<HouseholdChemical> chemicals = new ArrayList<>();
    /**
     * Categories list.
     */
    private List<String> categories = new ArrayList<>();
    /**
     * Last chemical id for setting new product id.
     */
    private Long lastId;
    /**
     * Singleton instance.
     */
    private static volatile StoreService instance;

    /**
     * Gets singleton instance if exists.
     * If not exists, create new and returns.
     *
     * @return singleton instance
     */
    public static StoreService getInstance() {
        StoreService localInstance = instance;
        if (localInstance == null) {
            synchronized (StoreService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = new StoreService();
                    instance = localInstance;
                }
            }
        }
        return localInstance;
    }

    /**
     * Add new chemical to list.
     *
     * @param chemical new product
     */
    public final void addChemical(final HouseholdChemical chemical) {
        chemicals.add(chemical);
        if (lastId == null) {
            lastId = 0L;
        }
        chemical.setId(lastId + 1);
        lastId++;

        if (!categories.contains(chemical.getCategory())) {
            categories.add(chemical.getCategory());
        }
    }

    /**
     * Gets all the chemicals.
     *
     * @return chemicals list
     */
    public final List<HouseholdChemical> getAll() {
        return chemicals;
    }

    /**
     * Gets all chemicals filtered by category.
     *
     * @param category chemical category
     * @return chemicals list filtered by category
     */
    public final List<HouseholdChemical> getByCategory(final String category) {
        return chemicals.stream()
                .filter(chemical -> chemical.getCategory().equals(category))
                .collect(Collectors.toList());
    }

    /**
     * Gets all chemicals filtered by category and sorted by price.
     *
     * @param category chemical category
     * @return chemicals list filtered by category and sorted by price.
     */
    public final List<HouseholdChemical> getByCategorySortedByPrice(
            final String category) {
        return chemicals.stream()
                .filter(chemical -> chemical.getCategory().equals(category))
                .sorted(Comparator.comparingDouble(HouseholdChemical::getPrice))
                .collect(Collectors.toList());
    }

    /**
     * Removes chemical from list by id.
     *
     * @param id id of chemical
     * @return true if chemical was found and removed,
     * false if chemical was not found by id
     */
    public final boolean removeById(final Long id) {
        Iterator<HouseholdChemical> iterator = chemicals.iterator();
        while (iterator.hasNext()) {
            HouseholdChemical next = iterator.next();
            if (next.getId().equals(id)) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    /**
     * Gets all categories.
     *
     * @return list of categories
     */
    public final String[] getCategories() {
        return categories.toArray(new String[0]);
    }

    /**
     * Gets chemical from list by id.
     *
     * @param id id of chemical.
     * @return chemical from list by id.
     */
    public final HouseholdChemical getById(final Long id) {
        return this.chemicals
                .stream()
                .filter(chemical -> chemical.getId().equals(id))
                .findFirst().get();
    }
}
