package com.khrystyna.models;

import java.util.Objects;

public class DishwashingDetergent extends HouseholdChemical {
    private String detergentType;
    private double capacity;

    public String getDetergentType() {
        return detergentType;
    }

    public void setDetergentType(String detergentType) {
        this.detergentType = detergentType;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DishwashingDetergent that = (DishwashingDetergent) o;
        return Double.compare(that.capacity, capacity) == 0 &&
                detergentType.equals(that.detergentType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(detergentType, capacity);
    }

    @Override
    public String toString() {
        return getBrand() + " " + capacity + " " + getPrice() + '$';
    }

    @Override
    public String toStringFull() {
        return "Brand: " + getBrand()
                + "\nCapacity: " + getCapacity() + 'L'
                + "\nDetergent type: " + getDetergentType()
                + "\nPrice: " + getPrice();
    }
}
