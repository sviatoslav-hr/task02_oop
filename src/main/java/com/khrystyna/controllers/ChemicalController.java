package com.khrystyna.controllers;

import com.khrystyna.models.HouseholdChemical;
import com.khrystyna.services.StoreService;

import java.util.List;

/**
 * An instance of this class is used to transfer data from database to view.
 */
public class ChemicalController {
    /**
     * An instance of mock database.
     */
    private StoreService storeService = StoreService.getInstance();

    /**
     * Gets array of names of every present category.
     *
     * @return an array of category names
     */
    public String[] getCategories() {
        return storeService.getCategories();
    }

    /**
     * Gets collection of all objects stored in database.
     *
     * @return list of all stored chemicals
     */
    public List<HouseholdChemical> getAllChemicals() {
        return storeService.getAll();
    }

    /**
     * Gets list of chemicals filtered by category from database.
     *
     * @param category name of category
     * @return list of chemicals filtered by category
     */
    public List<HouseholdChemical> getChemicalsByCategory(String category) {
        return storeService.getByCategorySortedByPrice(category);
    }

    /**
     * Gets Chemical instance from database by id field.
     *
     * @param id chemical id
     * @return single Chemical instance
     */
    public HouseholdChemical getChemicalById(long id) {
        return storeService.getById(id);
    }

    /**
     * Processes order by removing record from database and returns boolean result.
     *
     * @param id chemical id
     * @return true if successfully process order and chemical was removed,
     * false if chemical was not found by id
     */
    public boolean processOrder(long id) {
        return storeService.removeById(id);
    }
}
